"use client";
import {DataGridPremium, GridToolbar, GridApi, useGridApiRef} from "@mui/x-data-grid-premium";
import {Box} from "@mui/material";
import React from "react";
import {onSnapshot, doc, updateDoc, setDoc, getFirestore, collection} from "firebase/firestore";
import {db, units} from "../public/firebase";
import {objectToColumns, objectToRows} from "./dataTransformations";

export default function DataGridClient(props){

    const apiRef = useGridApiRef()
    const [rows, setRows] = React.useState(props.rows)
    const [columns, setColumns] = React.useState(props.columns)

    React.useEffect(()=> {
        const unsubscribe = onSnapshot(units, (querySnapshot) => {
            const info = {};
            querySnapshot.forEach((doc) => {
                info[doc.id] = doc.data()
            });
            setRows(objectToRows((info)))
            setColumns(objectToColumns(info).sort((a, b) => {return a.field.charCodeAt(0) - b.field.charCodeAt(0)}))


        });

        return () => {unsubscribe()}
    }, [])

    function getDetailPanelContent(){
        return(<h1>d</h1>)
    }

    async function processRowUpdate(newCellState, oldCellState){
        return new Promise(async function(resolve, reject) {
            for(let key in newCellState){
                if (newCellState[key] !== oldCellState[key]) {
                    const docRef = doc(db, "units/"+newCellState.id);
                    let update = { }
                    update[key] = newCellState[key]
                    await updateDoc(docRef, update).then((x) => resolve(newCellState))
                }
            }
            resolve(oldCellState)
        })
    }





    return(
            <>
            <Box sx={{height:"100vh", width:"100%"}}>

                <DataGridPremium
                    apiRef={apiRef}
                    onAggregationModelChange={()=>console.log(apiRef.current.exportState())}
                    initialState={{
                    columns: {
                        columnVisibilityModel: {
                            autopay: false,
                            emails: false,
                            timing: false,
                            url: false,
                            name: false,
                            "__detail_panel_toggle__": false,
                            "__reorder__": false
                        },
                    },
                        aggregation: {
                        model:{
                            value: "sum"
                        }
                    }
                }}
                    columns={columns}
                    rows={rows}
                    processRowUpdate={processRowUpdate}
                    components={{Toolbar: GridToolbar}}
                    checkboxSelection
                    density ="compact"
                    rowReordering={true}
                    experimentalFeatures={{aggregation: true, newEditingApi: true}}
                    getDetailPanelContent={getDetailPanelContent}
                    getDetailPanelHeight={() => "auto"}
                />
            </Box>

            <Box sx={{height:"100vh", width:"100%"}}>

                <DataGridPremium
                    apiRef={apiRef}
                    onAggregationModelChange={()=>console.log(apiRef.current.exportState())}
                    initialState={{
                    columns: {
                        columnVisibilityModel: {
                            autopay: false,
                            emails: false,
                            timing: false,
                            url: false,
                            name: false,
                            "__detail_panel_toggle__": false,
                            "__reorder__": false
                        },
                    },
                        aggregation: {
                        model:{
                            value: "sum"
                        }
                    }
                }}
                    columns={columns}
                    rows={rows}
                    processRowUpdate={processRowUpdate}
                    components={{Toolbar: GridToolbar}}
                    checkboxSelection
                    density ="compact"
                    rowReordering={true}
                    experimentalFeatures={{aggregation: true, newEditingApi: true}}
                    getDetailPanelContent={getDetailPanelContent}
                    getDetailPanelHeight={() => "auto"}
                />
            </Box>
            </>
            )
}