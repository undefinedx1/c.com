import { initializeApp } from "firebase/app";
import { getFirestore, query, collection, orderBy } from "firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyBHJ-GeV76tUKtDlfgdczCQNGMGZW5VW0w",
    authDomain: "cybernetic-stream-000.firebaseapp.com",
    projectId: "cybernetic-stream-000",
    storageBucket: "cybernetic-stream-000.appspot.com",
    messagingSenderId: "228862590973",
    appId: "1:228862590973:web:d94d750a230f0fceef6394"
};


export const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);

export const units = query(collection(db, "units"), orderBy("value", "desc"));

