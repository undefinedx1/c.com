import React from "react";
import {objectToColumns, objectToRows} from "../components/dataTransformations";
import DataGridBody from "../components/DataGridBody";

export async function getInfo(){
    const info = fetch("https://www.baron.miami/api", { next: { revalidate: 1 } }).then(x => x.json())
    return info
}


export default async function Page(){
    const info = await getInfo()

    return(
            <>
            <DataGridBody
                rows = {objectToRows(info)}
                columns = {objectToColumns(info).sort((a, b) => {return a.field.charCodeAt(0) - b.field.charCodeAt(0)}) }
            />
            </>
            )
}
