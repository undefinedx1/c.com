"use client";
import "./globals.css"
import React from "react";
import localFont from '@next/font/local';
import {useUserAgent} from 'next-useragent';
import {ThemeContext} from "../components/ThemeContext.js"
const myFont = localFont({ src: 'GoldmanSans_Th.ttf' });
import { createTheme, ThemeProvider } from '@mui/material/styles';

export default function RootLayout({children}) {

  const [ua, setUa] = React.useState(null)

  React.useMemo(() => {
      if (typeof window != "undefined") {
          setUa(useUserAgent(window.navigator.userAgent))
      }
    },[])

    return (
            <html lang="en" className={myFont.className}>
                <head/>
                <body>
                    <ThemeContext.Provider value={ua}>
                        <ThemeProvider theme={theme}>
                        {children}
                        </ThemeProvider>
                    </ThemeContext.Provider>
                </body>
            </html>
            )
}

const theme = createTheme({
    palette: {
        primary: {
            main: "#000000",
        }
    },
    typography: {
        fontFamily: "var(--font-inter)",
    }
});